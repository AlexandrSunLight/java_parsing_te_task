package main.java.LittleParser;

import java.io.*;

public class Extractor {

    static int filesCounter;

    public Extractor() {
        filesCounter = 1;
    }

    public static void main(String[] args){
        try {
            File directory = new File(args[1]);
            File[] fileArray = directory.listFiles();
            File newFile = new File(directory.getAbsolutePath() + "\\" + args[2]);
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile));
            Extractor extractor = new Extractor();
            String regexStr = "^.*" + args[0] + ".*$";
            for (File entry : fileArray)
            {
                extractor.extractionProcess(entry, bufferedWriter, regexStr);
            }
            bufferedWriter.close();
            System.out.println("Done!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void extractionProcess(File file, BufferedWriter bufferedWriter, String regexStr){
        String strBuffer;
        System.out.println("Reading file #" + filesCounter);
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            while (bufferedReader.ready()){
                strBuffer = bufferedReader.readLine();
                if(strBuffer.matches(regexStr)){
                    bufferedWriter.write(strBuffer);
                    bufferedWriter.newLine();
                }
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        filesCounter++;
    }

}
