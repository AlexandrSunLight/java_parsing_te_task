package main.java.LittleParser;

import java.io.*;

public class Splitter {

    public static int numSplits;
    private static long sourceSize;
    private static long bytesPerSplit;
    private static long remainingBytes;

    public static void main(String[] args){
        fileInfo(args[0]);
        String strBuffer;
        String newFileName;
        long bufferSize = 0;
        System.out.println("Hi! I'm cute little log splitter.");
        System.out.println("I'm ready to split your file! The number of parts is " + numSplits);
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(args[0]));
            int index = args[0].lastIndexOf(File.separator);
            System.out.println("Reading file...");
            for (int i = 0; i < numSplits; i++){
                System.out.println("Writing part #" + (i + 1) + "...");
                newFileName = String.format("%s%s_%02d",args[0].substring(0, index + 1), args[1], i + 1);
                File newFile = new File(newFileName);
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile));
                while (bufferedReader.ready() && bufferSize < bytesPerSplit) {
                    strBuffer = bufferedReader.readLine();
                    bufferedWriter.write(strBuffer);
                    bufferedWriter.newLine();
                    bufferSize += strBuffer.length();
                }
                System.out.println("Done!");
                bufferSize = 0;
                bufferedWriter.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Have a great day!");
    }

    private static void fileInfo(String fileName){
        numSplits = (int) (Math.random() * 5 + 5);

        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(fileName, "r");
            sourceSize = raf.length();
            raf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        bytesPerSplit = sourceSize/numSplits;
        remainingBytes = sourceSize % numSplits;
        if (remainingBytes != 0){
            bytesPerSplit += remainingBytes;
        }
    }
}
