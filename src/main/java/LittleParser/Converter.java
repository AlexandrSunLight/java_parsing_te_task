package main.java.LittleParser;

import java.io.*;

public class Converter {
    static int filesCounter;

    public Converter() {
        filesCounter = 1;
    }

    public static void main(String[] args){
        try {
            File directory = new File(args[1]);
            File[] fileArray = directory.listFiles();
            File newFile = new File(directory.getAbsolutePath() + "\\" + args[2] + ".csv");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile));
            Converter converter = new Converter();
            for (File entry : fileArray)
            {
                converter.conversionProcess(entry, bufferedWriter, args[0]);
            }
            bufferedWriter.close();
            System.out.println("Done!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void conversionProcess(File file, BufferedWriter bufferedWriter, String separatorType){
        String strBuffer;
        System.out.println("Reading file #" + filesCounter);
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            while (bufferedReader.ready()){
                strBuffer = bufferedReader.readLine();
                String [] strArray = strBuffer.split("\t");
                strArray[0] = strArray[0].replaceAll(separatorType, ";");
                strArray[0] += ";";
                for (String s : strArray) {
                    bufferedWriter.write(s);
                }
                bufferedWriter.newLine();
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        filesCounter++;
    }
}
